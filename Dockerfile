FROM python:3-alpine
ENV user=admin email=admin@example.com password=pass file=db/db.sqlite3

WORKDIR code
COPY . .

RUN pip install -r requirements.txt

RUN mkdir db
RUN ["python3", "manage.py", "makemigrations"]
RUN ["python3", "manage.py", "migrate"]
RUN echo "from django.contrib.auth.models import User; User.objects.create_superuser('$user', '$email', '$password')" | python3 manage.py shell

EXPOSE 8000
ENTRYPOINT ["python3", "manage.py", "runserver", "0.0.0.0:8000"]
